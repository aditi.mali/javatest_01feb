package javaprogramQ2e;

public class Area extends Shape{

	@Override
	public void RectangleArea(int length, int breadth) {
		// TODO Auto-generated method stub
		  int rectArea=length * breadth;
		  System.out.println("Area of Rectangle:"+rectArea);
	}

	@Override
	public void SquareArea(int side) {
		// TODO Auto-generated method stub
		int squArea=side * side;
		  System.out.println("Area of Square:"+squArea);
	}

	@Override
	public void CircleArea(double radius) {
		// TODO Auto-generated method stub
		double circleArea=3.14 * radius *radius;
		  System.out.println("Area of circle:"+circleArea);
		
	}
	
	
	public static void main(String[] args) {
		 Area a= new Area();
		 a.RectangleArea(10, 20);
		 a.SquareArea(10);
		 a.CircleArea(30);
	}

}
