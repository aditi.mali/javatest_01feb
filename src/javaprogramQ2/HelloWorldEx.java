package javaprogramQ2;

public class HelloWorldEx {
	
	public static void main(String[] args)
	{
	    String string = "Hello World";
	    
	    int first = string.indexOf("o");
	    int last = string.lastIndexOf("o");
	    System.out.println("First occurrence of letter 'o': " + first);
	    System.out.println("Last occurrence of letter 'o': " + last);
	  }

}
