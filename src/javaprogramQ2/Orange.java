package javaprogramQ2;

import java.util.Scanner;

public class Orange {
	
	public static void main(String[] args)
	{
		
		String str = "This is orange juice";

		if (str.contains("orange")) 
		{
			System.out.println("The word 'orange' is present");
		} 
		else 
		{
			System.out.println("The word 'orange' is not present");
		}
	}

}
