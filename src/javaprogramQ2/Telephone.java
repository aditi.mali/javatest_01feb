package javaprogramQ2;

public abstract class Telephone 
{

	public abstract void with();
	public abstract void lift();
	public abstract void disconnected();
	
}
