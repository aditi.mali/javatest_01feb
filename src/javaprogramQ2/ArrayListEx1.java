package javaprogramQ2;

import java.util.ArrayList;

public class ArrayListEx1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		ArrayList<String> al= new ArrayList<String>();
		System.out.println("Arraylist:"+al);
		//System.out.println(al.size());
		al.add("aaaa");
		al.add("bbbbb");
		al.add("ccccc");
		al.add("Coke");
		al.add("ddddd");
		
		System.out.println(al.size());
		System.out.println(al);
		
		al.remove(1);
		System.out.println("After removing element:"+al);
		boolean s=al.contains("Coke");
		System.out.println("Coke is present:"+s);
	}

}
